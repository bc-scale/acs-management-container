#!/bin/sh

set -euo pipefail

acs_log() {
	local type="${1}"; shift
	local text="${*}"; [ "${#}" -eq "0" ] && text="$(cat)"
	local now="$(date --rfc-3339=seconds)"
	printf '%s [%s] [Entrypoint]: %s\n' "${now}" "${type}" "${text}"
}

acs_note() {
	acs_log NOTE "${@}"
}

acs_warn() {
	acs_log WARN "${@}" 1>&2
}

acs_error() {
	acs_log ERROR "${@}" 1>&2
	exit 1
}

file_env() {
	local var="${1}"
	local file_var="${var}_FILE"
	if [ "x${!var:-}" != "x" -a "x${!file_var:-}" != "x" ]; then
		acs_error "Both ${var} and ${file_var} is set (but are exclusive)."
	fi
	local val="${2:-}"
	if [ "x${!var:-}" != "x" ]; then
		val="${!var}"
	elif [ "x${!file_var:-}" != "x" ]; then
		val="$(cat ${!file_var})"
	fi
	export "${var}=${val}"
	unset "${file_var}"
}

svc_env() {
	local var="${1}"
	local service_file="$(find / -name cloudstack-management.service 2>/dev/null | head -1 || true)"
	local val=$(grep "${var}=" "${service_file}" | cut -d'=' -f2)
	export "${var}=${val}"
}

acs_setup_db() {
	acs_note "Configure database..."
	local db="${1}"; shift
	local mgmtkey="${1}"; shift
	local dbkey="${1}"; shift
	local mgmtip="${1}"; shift
	cloudstack-setup-databases \
		"${db}" \
		-e file \
		-m "${mgmtkey}" \
		-k "${dbkey}" \
		-i "${mgmtip}" $@
}

acs_note "Cloudstack Management v4.17.1.0 entrypoint."

if [ "${1}" = "acs-management" ]; then
	trap 'acs_note "Exiting..."; exit 0' 1 2 3 6 9

	file_env "ACS_MANAGEMENT_KEY" "cloudstack"
	file_env "ACS_MANAGEMENT_IP" "127.0.0.1"
	file_env "ACS_DB" "cloud:cloud@mysql:3306"
	file_env "ACS_DB_SECRET" "cloudstack"
	#file_env "ACS_DB_ROOT_PASSWORD"

	#set -- "${ACS_DB}" -e file -m "${ACS_MANAGEMENT_KEY}" -k "${ACS_DB_SECRET}" -i "${ACS_MANAGEMENT_IP}"
	#if [ "x${ACS_DB_ROOT_PASSWORD:-}" != "x" ]; then
	#	set -- "$@" --deploy-as="root:${ACS_DB_ROOT_PASSWORD}"
	#fi

	#cloudstack-setup-databases "$@"

	acs_setup_db "${ACS_DB}" "${ACS_MANAGEMENT_KEY}" "${ACS_DB_SECRET}" "${ACS_MANAGEMENT_IP}"
	unset ACS_MANAGEMENT_KEY
	unset ACS_MANAGEMENT_IP
	unset ACS_DB
	unset ACS_DB_SECRET

	acs_note "Setup CloudStack Management Server..."
	eval cloudstack-setup-management --no-start

	svc_env "ExecStart"
	svc_env "EnvironmentFile"
	svc_env "WorkingDirectory"

	acs_note "Starting CloudStack Management Server..."
	exec  /bin/sh -c ". ${EnvironmentFile}; cd ${WorkingDirectory}; ${ExecStart}"
else
	exec "$@"
fi
