# Apache CloudStack Management Container Image

Apache CloudStack Management Container Image is a tool that allows user of <abbr title="Apache CloudStack">acs</abbr>
to easily spawn management server.

Container is based on `rocky` linux and use official CloudStack repositories.

## Prerequisities

Before you begin, ensure you have met this following requirements:

- You have a mysql server up and running
- You have a initialized `cloud` database (TODO: Should be done by this project)
- You have a Linux machine with containerization installed

## Getting Apache CloudStack Management Container Image

To get Apache CloudStack Management Container Image just run:

```
docker pull registry.gitlab.com/phonexia-org/acs-management-container:4.17.1.0
```

## Using Apache CloudStack Management Container Image

To use Apache CloudStack Management Container Image, follow these steps:

In basic usage you need to link database into this container. For this purpose can be use [legacy --link](https://docs.docker.com/network/links), use [same network for both containers](https://docs.docker.com/network/bridge) and database network named mysql or just hack hostname `mysql` into `/etc/hostname`:
```
docker run -d --link database:mysql registry.gitlab.com/phonexia-org/acs-management-container:4.17.1.0
```

Advanced example template:
```
docker run -d \
  -e ACS_MANAGEMENT_KEY=<mgmt key defined during initialization> \
  -e ACS_MANAGEMENT_IP=<public ip of management server> \
  -e ACS_DB=<database connection with user and password> \
  -e ACS_DB_SECRET=<secret key defined during initialization> \
  -m 6000M \
  --cpus=1 \
  registry.gitlab.com/phonexia-org/acs-management-container:4.17.1.0
```

## Contributing to Apache CloudStack management Container Image

To contribute to Apache CloudStack Management Container Image, follow these steps:

- We do not have a well-trodden path for contributions, so simple reach us at [ict@phonexia.com](mailto:ict@phonexia.com) and we'll make an arrangement

## Contributors

Thanks to the following people who have contributed to this project:

- @phonexia.josef.vecera
- @phonexia.jaromir.smejkal

## Contact

If you want to contact me you can reach us at [ict@phonexia.com](mailto:ict@phonexia.com).

## License

This project uses the following license: [Apache 2.0](LICENSE) .
