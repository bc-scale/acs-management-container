FROM rockylinux:8.6

ARG ACS_VERSION=4.17.1.0
ARG ACS_MAJOR_VERSION=4.17

RUN dnf -y update && dnf -y install 'dnf-command(config-manager)' && \
	dnf config-manager --add-repo "http://download.cloudstack.org/el/\$releasever/${ACS_MAJOR_VERSION}" && \
	dnf config-manager --save --setopt=*cloudstack*.gpgcheck=0 && \
	dnf -y update && dnf -y install chrony cloudstack-management-${ACS_VERSION} && \
	find /usr/share/cloudstack-management/templates/systemvm/ -type f -not -name 'metadata.ini' -delete && \
	rm -rf /var/cache/dnf/*

COPY docker-entrypoint.sh /

EXPOSE 8080 8443 8250 9090

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["acs-management"]
